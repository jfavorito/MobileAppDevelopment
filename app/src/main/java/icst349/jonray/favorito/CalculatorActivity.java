package icst349.jonray.favorito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static icst349.jonray.favorito.R.id.display;


public class CalculatorActivity extends AppCompatActivity {
    private TextView display;
    private static final String KEY_TEXT_VALUE = "textValue";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        display = (TextView)findViewById(R.id.display);
        if (savedInstanceState != null)
        {
            CharSequence savedText = savedInstanceState.getCharSequence(KEY_TEXT_VALUE);
            display.setText(savedText);
        }
        Button b[] = new Button[10];;
        Button dzero = (Button)findViewById(R.id.num00);
        Button delete = (Button)findViewById(R.id.delete);
        Button decimal = (Button)findViewById(R.id.decimal);
        Button bCalc = (Button)findViewById(R.id.PreviousCalc);
        Button nCalc = (Button)findViewById(R.id.NextCalc);
        Button equal = (Button)findViewById(R.id.equal);
        Button add = (Button)findViewById(R.id.add);
        Button subtract = (Button)findViewById(R.id.subtract);
        Button multiply = (Button)findViewById(R.id.multiply);
        Button divide = (Button)findViewById(R.id.divide);
        Button clear = (Button)findViewById(R.id.clear);
        b[1] = (Button)findViewById(R.id.num1);
        b[2] = (Button)findViewById(R.id.num2);
        b[3] = (Button)findViewById(R.id.num3);
        b[4] = (Button)findViewById(R.id.num4);
        b[5] = (Button)findViewById(R.id.num5);
        b[6] = (Button)findViewById(R.id.num6);
        b[7] = (Button)findViewById(R.id.num7);
        b[8] = (Button)findViewById(R.id.num8);
        b[9] = (Button)findViewById(R.id.num9);
        b[0] = (Button)findViewById(R.id.num0);

        dzero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String o = "00";
                display.append("00");
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String temp = display.getText().toString();
                temp = temp.substring(0, temp.length() - 1);
                display.setText(temp);
            }
        });
        decimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                char p = '.';
                String po = String.valueOf(p);
                display.append(po);
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText("");
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                char operator;
                operator = '+';
                String op = String.valueOf(operator);
                display.append(op);
            }
        });

        subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                char operator;
                operator = '-';
                String op = String.valueOf(operator);
                display.append(op);
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                char operator;
                operator = '*';
                String op = String.valueOf(operator);
                display.append(op);
            }
        });

        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                char operator;
                operator = '/';
                String op = String.valueOf(operator);
                display.append(op);
            }
        });

        for (int i = 0; i < 10; i++)
        {
            final int c = i;
            b[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String number = String.valueOf(c);
                    display.append(number);
                }
            });
        }

        bCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent bc = new Intent(CalculatorActivity.this, EditProfileActivity.class);
                startActivity(bc);
            }
        });

        nCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nc = new Intent(CalculatorActivity.this, MessengerActivity.class);
                startActivity(nc);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outstate){
        super.onSaveInstanceState(outstate);
        outstate.putCharSequence(KEY_TEXT_VALUE, display.getText());
    }
}
