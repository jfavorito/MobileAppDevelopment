package icst349.jonray.favorito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EditProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);

        Button backEdit = (Button)findViewById(R.id.PreviousEdit);
        Button nextEdit = (Button)findViewById(R.id.NextEdit);

        backEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent bEdit = new Intent(EditProfileActivity.this, FriendsListActivity.class);
                startActivity(bEdit);
            }
        });

        nextEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nEdit = new Intent(EditProfileActivity.this, CalculatorActivity.class);
                startActivity(nEdit);
            }
        });
    }

    /*

    */
}
