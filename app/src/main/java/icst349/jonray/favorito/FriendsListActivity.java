package icst349.jonray.favorito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FriendsListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);
        Button b = (Button)findViewById(R.id.PreviousFriends);
        Button n = (Button)findViewById(R.id.NextFriends);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ba = new Intent(FriendsListActivity.this, RatingBarActivity.class);
                startActivity(ba);
            }
        });

        n.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ne = new Intent(FriendsListActivity.this, EditProfileActivity.class);
                startActivity(ne);
            }
        });
    }
}
