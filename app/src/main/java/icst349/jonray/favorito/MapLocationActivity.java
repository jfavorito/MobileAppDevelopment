package icst349.jonray.favorito;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MapLocationActivity extends AppCompatActivity {

    EditText location;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_location);
        Button button = (Button)findViewById(R.id.findButton);
        location = (EditText)findViewById(R.id.searchLocale);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String a = location.getText().toString();
                String map = "http://maps.google.co.in/maps?q=" + a;
                Intent callMap = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                startActivity(callMap);
            }
        });
    }
}
