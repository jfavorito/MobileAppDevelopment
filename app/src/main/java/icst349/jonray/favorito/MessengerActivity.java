package icst349.jonray.favorito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MessengerActivity extends AppCompatActivity {


    private EditText receiver;
    private EditText message;
    Button sendmsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);
        receiver = (EditText) findViewById(R.id.recipient);
        message = (EditText) findViewById(R.id.message);
        Button nMessage = (Button)findViewById(R.id.NextMess);
        Button bMessage = (Button)findViewById(R.id.PreviousMess);
        receiver.addTextChangedListener(textWatcher);
        message.addTextChangedListener(textWatcher);

        checkFields();

        sendmsg.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View view){
                        TextView thread = (TextView) findViewById(R.id.thread);
                        String to = "To: ";
                        String strmessage = message.getText().toString();
                        String from = "From: ";
                        String recipient = receiver.getText().toString();
                        String end = "\n";
                        String toS = to + recipient + end + strmessage + end;
                        String din = " din\n";
                        String msg = from + recipient + end + strmessage + din;
                        String finMessage = toS + msg;


                        thread.append(finMessage);
                    }
                }
        );

        bMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent bmess = new Intent(MessengerActivity.this, CalculatorActivity.class);
                startActivity(bmess);
            }
        });

        nMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nMess = new Intent(MessengerActivity.this, RandomizerActivity.class);
                startActivity(nMess);
            }
        });
    }


    private void checkFields()
    {
        sendmsg = (Button) findViewById(R.id.send);
        String r = receiver.getText().toString();
        String m = message.getText().toString();

        if (r.equals("") || m.equals(""))
            sendmsg.setEnabled(false);
        else
            sendmsg.setEnabled(true);
    }


    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            checkFields();
        }
    };

}
