package icst349.jonray.favorito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Random;

public class RandomizerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_randomizer);
        LinearLayout screen = (LinearLayout) findViewById(R.id.screen1);
        Button bRandom = (Button)findViewById(R.id.PreviousRandom);
        Button nRandom = (Button)findViewById(R.id.NextRandom);
        final NumberPicker picker = (NumberPicker) findViewById(R.id.picker);

        picker.setMinValue(1);
        picker.setMaxValue(12);
        picker.setWrapSelectorWheel(true);

        screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //function
                TextView textView = (TextView) findViewById(R.id.textView);
                int n = picker.getValue();
                Random r = new Random();
                int newNum = r.nextInt(n) + 1;
                String number = String.valueOf(newNum);
                textView.setText(number);
            }
        });

        bRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent brand = new Intent(RandomizerActivity.this, MessengerActivity.class);
                startActivity(brand);
            }
        });

        nRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nRand = new Intent(RandomizerActivity.this, RatingBarActivity.class);
                startActivity(nRand);
            }
        });
    }
}
