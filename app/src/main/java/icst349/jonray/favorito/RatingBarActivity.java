package icst349.jonray.favorito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RatingBarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_bar);
        Button back = (Button)findViewById(R.id.PreviousRating);
        Button next = (Button)findViewById(R.id.NextRating);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent b = new Intent(RatingBarActivity.this, RandomizerActivity.class);
                startActivity(b);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(RatingBarActivity.this, FriendsListActivity.class);
                startActivity(n);
            }
        });
    }
}
